Admin and user config dashboard for WotBot https://codeberg.org/vanous/wotbot

Running version of this WotBot Dashboard is available here: https://wotbot.pythonanywhere.com/

Development server and community: https://discord.gg/mzXYPVW

Licensed under GNU Affero General Public License version 3



Oauth2 code scaffold taken from discord example (https://github.com/discordapp/discord-oauth2-example)

This project uses shoulders of these giants:

python, python-flask

Rivets.js for two-way data bindings.

Bootstrap for web theme

