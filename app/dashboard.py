import os
from flask import (
    Flask,
    g,
    session,
    redirect,
    request,
    url_for,
    jsonify,
    render_template,
)
from requests_oauthlib import OAuth2Session
from flask_wtf.csrf import CSRFProtect

# import aiohttp
# import asyncio
import requests
import json
import sqlite3
from flask_babel import Babel, gettext, ngettext, _
import datetime

# from flask_babel import lazy_gettext as _l

from app import app

csrf = CSRFProtect(app)

with open("./data/config.json") as json_data_file:
    cfg = json.load(json_data_file)

LANGUAGES = {
    "en": "English",
    "cs": "Czech",
    "nl": "Dutch",
    "ru": "Russian",
    "fr": "French",
    "de": "German",
    "az": "Azerbaijani",
    "hu": "Hungarian",
    "pl": "Polish",
    "it": "Italian",
    "es": "Spanish",
    "ar": "Arabic",
    "tr": "Turkish",
    "zh": "Chinese",
    "uk": "Ukrainian",
}


@app.before_request
def before_request():
    if not request.is_secure and cfg["FLASK_DEBUG"] != "True":
        url = request.url.replace("http://", "https://", 1)
        code = 301
        return redirect(url, code=code)


@app.context_processor
def inject_languages():
    return dict(lang=LANGUAGES)


TEMPLATES_AUTO_RELOAD = cfg["TEMPLATES_AUTO_RELOAD"]
OAUTH2_CLIENT_ID = cfg["OAUTH2_CLIENT_ID"]
OAUTH2_CLIENT_SECRET = cfg["OAUTH2_CLIENT_SECRET"]
OAUTH2_REDIRECT_URI = cfg["OAUTH2_REDIRECT_URI"]
API_BASE_URL = "https://discordapp.com/api"
AUTHORIZATION_BASE_URL = API_BASE_URL + "/oauth2/authorize"
TOKEN_URL = API_BASE_URL + "/oauth2/token"
WEBHOOK_CHANNEL = cfg["WEBHOOK_CHANNEL"]
WEBHOOK_TOKEN = cfg["WEBHOOK_TOKEN"]
WEBHOOK_URL = API_BASE_URL + "/webhooks/" + WEBHOOK_CHANNEL + "/" + WEBHOOK_TOKEN
DASHBOARD_KEY = cfg["DASHBOARD_KEY"]
WARGAMING_TOKEN = cfg["WARGAMING_TOKEN"]
WARGAMING_URL = cfg["WARGAMING_URL"]
WARGAMING_REDIRECT_URL = cfg["WARGAMING_REDIRECT_URL"]
WG_URL = WARGAMING_URL.format(
    app_id=WARGAMING_TOKEN, redirect_url=WARGAMING_REDIRECT_URL, region={}
)

# app = Flask(__name__)
app.debug = cfg["FLASK_DEBUG"]
app.config["SECRET_KEY"] = cfg["SECRET_KEY"]

babel = Babel(app)

if "http://" in OAUTH2_REDIRECT_URI:
    os.environ["OAUTHLIB_INSECURE_TRANSPORT"] = cfg["OAUTHLIB_INSECURE_TRANSPORT"]


@babel.localeselector
def get_locale():
    # return request.accept_languages.best_match(['de', 'fr', 'en'])
    # return request.accept_languages.best_match(LANGUAGES.keys())
    # return "cs_CZ"
    try:
        language = session["language"]
    except KeyError:
        language = None
    if language is not None:
        if language in LANGUAGES:
            return language
    return request.accept_languages.best_match(LANGUAGES.keys())


@app.route("/language/<language>")
def set_language(language=None):
    if language in LANGUAGES:
        session["language"] = language
    return redirect(url_for("index"))


@babel.timezoneselector
def get_timezone():
    user = getattr(g, "user", None)
    if user is not None:
        return user.timezone


def token_updater(token):
    session["oauth2_token"] = token


def make_session(token=None, state=None, scope=None):
    return OAuth2Session(
        client_id=OAUTH2_CLIENT_ID,
        token=token,
        state=state,
        scope=scope,
        redirect_uri=OAUTH2_REDIRECT_URI,
        auto_refresh_kwargs={
            "client_id": OAUTH2_CLIENT_ID,
            "client_secret": OAUTH2_CLIENT_SECRET,
        },
        auto_refresh_url=TOKEN_URL,
        token_updater=token_updater,
    )


@app.route("/")
def index():
    # discord = make_session(token=session.get('oauth2_token'))

    if not session.get("username", False):
        return render_template("index_guest.html")

    user_id = session.get("user_id")
    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=? AND deleted IS NOT 1", (user_id,))
        row = cur.fetchone()
    if row:
        return render_template(
            "index.html",
            id=json.loads(row[0]),
            user=json.loads(row[1]),
            guilds=json.loads(row[2]),
            blitzquiz=json.loads(row[4]),
        )
    else:
        return render_template(
            "wait.html", url="index", name=_("Continue to Dashboard")
        )


@app.route("/logout")
def logout():
    # discord = make_session(token=session.get('oauth2_token'))
    if session.get("oauth2_state", None):
        session.pop("oauth2_state")

    if session.get("oauth2_token", None):
        session.pop("oauth2_token")
    if session.get("username", None):
        session.pop("username")
    if session.get("user_id", None):
        user_id = session.pop("user_id")
        if user_id:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute("delete from  data where id = (?)", (user_id,))
                con.commit()
    # return render_template('index.html')
    return redirect(url_for("index"))


@app.route("/login")
def login():
    scope = request.args.get("scope", "identify guilds")
    discord = make_session(scope=scope.split(" "))
    authorization_url, state = discord.authorization_url(AUTHORIZATION_BASE_URL)
    session["oauth2_state"] = state
    return redirect(authorization_url)


@app.route("/callback")
def callback():
    if request.values.get("error"):
        return redirect(url_for("index"))
        # return request.values['error']
    discord = make_session(state=session.get("oauth2_state"))
    try:
        token = discord.fetch_token(
            TOKEN_URL,
            client_secret=OAUTH2_CLIENT_SECRET,
            authorization_response=request.url,
        )
        session["oauth2_token"] = token
        return redirect(url_for("me"))
    except:
        return redirect(url_for("index"))


@csrf.exempt
@app.route("/receiver", methods=["POST"])
def receiver():
    data = request.get_json()
    if data:
        data_key = data.get("dashboard_key", False)
        if data_key == DASHBOARD_KEY:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute(
                    "INSERT or REPLACE INTO data (id,playerconfigs,guilds,guilds_invite,blitzquiz,time_stamp) VALUES (?,?,?,?,?,?)",
                    (
                        data["user_id"],
                        json.dumps(data["playerconfigs"]),
                        json.dumps(data["guilds"]),
                        json.dumps(data["guilds_invite"]),
                        json.dumps(data["blitzquiz"]),
                        datetime.datetime.utcnow(),
                    ),
                )
                con.commit()

    return "OK"


@csrf.exempt
@app.route("/responder", methods=["POST"])
def responder():
    data = request.get_json()
    if data:
        data_key = data.get("dashboard_key", False)
        if data_key == DASHBOARD_KEY:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute(
                    "select response from data where id=(?)", (data["user_id"],)
                )

            row = cur.fetchone()
            data = json.loads(row[0])
            data["content"]["dashboard_key"] = DASHBOARD_KEY
            return jsonify(data)


@app.route("/me")
def me():
    discord = make_session(token=session.get("oauth2_token"))
    user = discord.get(API_BASE_URL + "/users/@me").json()
    guilds = discord.get(API_BASE_URL + "/users/@me/guilds").json()
    # print(guilds)
    # connections = discord.get(API_BASE_URL + '/users/@me/connections').json()
    # return jsonify(user=user, guilds=guilds, connections=connections)
    if user.get("username", False):
        session["username"] = user.get("username")
        session["user_id"] = user.get("id")
    else:
        return redirect(url_for("login"))

    if session.get("user_id", None):
        user_id = session.get("user_id")
        if user_id:
            with sqlite3.connect("./data/database.db") as con:
                cur = con.cursor()
                cur.execute("update data set deleted = 1 where id = (?)", (user_id,))
                con.commit()
    if type(guilds) is dict:
        return render_template("wait.html", url="me", name=_("Reload this page"))
    guilds_send = [
        {"id": x["id"], "name": x["name"]}
        for x in guilds
        if x["owner"] or x["permissions"] >> 3 & 1
    ]
    session["guilds"] = guilds_send
    content = {
        "command": "get-data",
        "username": session["username"],
        "dashboard_key": "fill me up",
        "user_id": session["user_id"],
        "guilds": guilds_send,
    }

    # payload = {"username": "Dashboard", "content": json.dumps(content)}
    payload = {"username": "Dashboard", "content": content}
    # save payload here
    w = {}
    w["payload"] = json.dumps(payload)
    w["id"] = session["user_id"]

    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        cur.execute("select * from data where id=$id",w)
        row = cur.fetchone()
        #print("___",row)
        if row:
            cur.execute( "update data set response=$payload where id=$id", w,)
        else:
            cur.execute( "insert into data (id, response,guilds,guilds_invite, playerconfigs,blitzquiz,deleted) values ($id, $payload,'{}','{}','{}','{}',1)", w,)
        cur.execute("select * from data where id=$id",w)
        row = cur.fetchone()
        #print("___",row)
        con.commit()

        # make new payload just with "fetch"

        content = {
            "command": "fetch",
            "dashboard_key": DASHBOARD_KEY,
            "user_id": session["user_id"],
        }
    payload = {"username": "Dashboard", "content": json.dumps(content)}
    #print("go")
    requests.post(WEBHOOK_URL, data=payload)

    return redirect(url_for("edit"))
    # return redirect(url_for('me'))


def post_data(request):

    user_id = session.get("user_id")

    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (user_id,))
        row = cur.fetchone()
    if not row:
        return redirect(url_for("login"))
    orig_guilds = json.loads(row[2])

    try:
        data = request.get_json(force=True)
    except:
        data = False
    #print("post data from request",data)
    if data:
        data["userid"] = user_id
        #skip this for now
        #if data.get("guilds", False):
        #    for guild in data.get("guilds"):
        #        if not [True for x in orig_guilds if guild["id"] in x.values()]:
        #            data["guilds"].remove(guild)
    else:
        data = {}
        data["userid"] = user_id
        if row[2]:
            guilds = json.loads(row[2])
            for g in guilds:
                g.pop("channels", False)

        data["guilds"] = guilds
        data["user"] = json.loads(row[1])

    data["user"]["confirmedname"] = session.get("wgnickname", None)
    data["user"]["confirmedregion"] = session.get("wgregion", "").replace("com", "na")
    data["user"]["confirmedtoken"] = session.get("wgaccess_token", None)
    data["user"]["confirmedaccount_id"] = session.get("wgaccount_id", None)

    #print("post data",data)
    content = {
        "command": "set-data",
        "username": session["username"],
        "user_id": session["user_id"],
        "dashboard_key": DASHBOARD_KEY,
        "data": data,
    }
    # "guilds": session["guilds"],
    # payload = {"username": "Dashboard", "content": json.dumps(content)}
    payload = {"username": "Dashboard", "content": content}
    # requests.post(WEBHOOK_URL, data=payload)

    # save payload here
    w = {}
    w["payload"] = json.dumps(payload)
    w["id"] = session["user_id"]
    w["timestamp"] = datetime.datetime.utcnow()

    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        cur.execute("update data set response=$payload, time_stamp=$timestamp where id=$id", w)
        con.commit()

        # make new payload just with "fetch"

        content = {
            "command": "fetch",
            "dashboard_key": DASHBOARD_KEY,
            "user_id": session["user_id"],
        }
    payload = {"username": "Dashboard", "content": json.dumps(content)}
    #print("go")
    requests.post(WEBHOOK_URL, data=payload)
    return "OK"

def post_remove_data():
    user_id = session.get("user_id")

    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=?", (user_id,))
        row = cur.fetchone()
    if not row:
        return redirect(url_for("login"))


    #print("post data",data)
    content = {
            "command": "remove-data",
            "dashboard_key": DASHBOARD_KEY,
            "confirmedaccount_id": session.get("wgaccount_id", None),
            "user_id": session["user_id"],
        }
    payload = {"username": "Dashboard", "content": json.dumps(content)}
    requests.post(WEBHOOK_URL, data=payload)

    if session["wgaccess_token"] is not None and session["wgregion"] is not "":
        #print(session["wgaccess_token"])
        print("clear wg data")
        url = "https://api.worldoftanks.{region}/wot/auth/logout/".format(
            region=session.get("wgregion", "").replace("na", "com")
        )
        payload={
                'access_token':session["wgaccess_token"],
                'application_id':WARGAMING_TOKEN
                }
        r = requests.post(url,data=payload)
        print(r.status_code)
        print(r.text)
    logout()
    return "OK"

@app.route("/data", methods=["GET", "POST"])
def data():
    if request.method == "GET":
        if not session.get("username", False):
            return redirect(url_for("login"))

        user_id = session.get("user_id")
        with sqlite3.connect("./data/database.db") as con:
            cur = con.cursor()
            # con.row_factory = sqlite3.Row
            cur.execute("select * from data where id=?", (user_id,))
            row = cur.fetchone()
        if row:
            return jsonify(
                userid=json.loads(row[0]),
                user=json.loads(row[1]),
                guilds=json.loads(row[2]),
                guilds_invite=json.loads(row[3]),
            )
        else:
            return jsonify(userid="", user="", guilds=[], guilds_invite=[])

    if request.method == "POST":
        if not session.get("username", False):
            return redirect(url_for("login"))

        try:
            data = request.get_json(force=True)
        except:
            data = False
        print(data)
        if data:
            action = data.get("action","")
            if "remove-data" in action:
                print("proceed")
                return post_remove_data()

        return post_data(request)


@app.route("/edit")
def edit():
    # print(session)
    if not session.get("username", False):
        return redirect(url_for("login"))
    user_id = session.get("user_id")
    with sqlite3.connect("./data/database.db") as con:
        cur = con.cursor()
        # con.row_factory = sqlite3.Row
        cur.execute("select * from data where id=? AND deleted IS NOT 1", (user_id,))
        row = cur.fetchone()
    if row:
        userdata = json.loads(row[1])
        # userdata=data.get("playerconfigs",{})
        # print("set initial data",userdata)
        session["wgnickname"] = userdata.get("confirmedname", None)
        session["wgaccount_id"] = userdata.get("confirmedaccount_id", None)
        session["wgregion"] = userdata.get("confirmedregion", "").replace("na", "com")
        session["wgaccess_token"] = userdata.get("confirmedtoken", None)
        # print("session set",session)

        if session["wgaccount_id"] is not None and session["wgregion"] is not "":
            # print(session["wgaccess_token"])
            url = "https://api.wotblitz.{region}/wotb/account/info/?application_id={wg_token}&account_id={account_id}&access_token={access_token}".format(
                region=session["wgregion"],
                account_id=session["wgaccount_id"],
                access_token=session["wgaccess_token"],
                wg_token=WARGAMING_TOKEN,
            )
            r = requests.get(url)
            res = json.loads(r.text)
            # print(res)
            if res.get("status", "error") != "ok":
                # print("not auth")
                # user = User(nickname=nickname,account_id =account_id ,access_token=access_token)
                # login_user(user, remember=access_token)
                session["wgaccess_token"] = None

            throw = post_data(request)

        return render_template(
            "edit.html",
            OAUTH2_CLIENT_ID=OAUTH2_CLIENT_ID,
            OAUTH2_REDIRECT_URI=OAUTH2_REDIRECT_URI,
            userid=json.loads(row[0]),
            user=json.loads(row[1]),
            guilds=json.loads(row[2]),
            guilds_invite=json.loads(row[3]),
            WG_URL=WG_URL,
        )
    else:
        return render_template("wait.html", url="edit", name=_("Continue to Settings"))


@app.route("/help")
def help():
    return render_template("help.html")


@app.route("/logoutwg")
def logoutwg():

    if not session.get("username", False):
        return redirect(url_for("login"))

    if session.get("wgstatus", False) == "ok":
        url = "https://api.worldoftanks.{region}/wot/auth/logout/".format(
            region=session.get("wgregion", "").replace("na", "com")
        )
        data = {
            "application_id": WARGAMING_TOKEN,
            "access_token": session.get("wgaccess_token", None),
        }
        r = requests.post(url, data=data)
        res = json.loads(r.text)
        print(res)

    session["wgregion"] = ""
    session["wgnickname"] = None
    session["wgaccount_id"] = None
    session["wgaccess_token"] = None
    session["wgstatus"] = ""

    throw = post_data(request)
    return render_template("wait.html", url="me", name=_("Reload this page"))


@app.route("/login_redirect", methods=["GET", "POST"])
def login_redirect():
    if not session.get("username", False):
        return redirect(url_for("login"))

    session["wgregion"] = ""
    session["wgnickname"] = None
    session["wgaccount_id"] = None
    session["wgaccess_token"] = None
    session["wgstatus"] = ""

    if not session.get("username", False):
        return redirect(url_for("login"))

    wgregion = request.args.get("region", "")
    wgstatus = request.args.get("status", None)
    wgaccount_id = request.args.get("account_id", None)
    wgnickname = request.args.get("nickname", None)
    wgaccess_token = request.args.get("access_token", None)

    # print(wgaccess_token)
    session["wgstatus"] = wgstatus
    if not wgstatus:
        return redirect(url_for("me"))

    if "error" not in wgstatus and wgaccount_id is not None and wgregion is not "":

        url = "https://api.wotblitz.{region}/wotb/account/info/?application_id={wg_token}&account_id={account_id}&access_token={access_token}".format(
            region=wgregion,
            account_id=wgaccount_id,
            access_token=wgaccess_token,
            wg_token=WARGAMING_TOKEN,
        )
        r = requests.get(url)
        res = json.loads(r.text)
        # print(res)
        # print(wgaccess_token)
        if res.get("status", None) == "ok":
            # user = User(nickname=nickname,account_id =account_id ,access_token=access_token)
            # login_user(user, remember=access_token)
            session["wgnickname"] = wgnickname
            session["wgaccount_id"] = wgaccount_id
            session["wgaccess_token"] = wgaccess_token
            session["wgregion"] = wgregion
            session["wgstatus"] = "ok"

    throw = post_data(request)
    return render_template("wait.html", url="me", name=_("Reload this page"))


if __name__ == "__main__":
    app.run()
