#!/bin/bash
source ../data/crowdin.config
pybabel extract -F babel.cfg -o messages.pot .
curl -F "files[/Dashboard/messages.pot]=@messages.pot" https://api.crowdin.com/api/project/WotBot/update-file?key=$key
