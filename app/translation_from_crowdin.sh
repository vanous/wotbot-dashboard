#!/bin/bash
source ../data/crowdin.config
echo "building project..."
curl https://api.crowdin.com/api/project/WotBot/export?key=$key
wget https://api.crowdin.com/api/project/WotBot/download/all.zip?key=$key -O all.zip
bsdtar -xf all.zip -s'|[^/]*/||' dashboard/translations/*
pybabel compile -d translations

